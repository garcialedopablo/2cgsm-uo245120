(window.onload = () => {
	
	"use strict"; // We always use strict mode

	var webGlAvailable = WEBGL.isWebGLAvailable() 
	if (!webGlAvailable) {
		alert("WebGL not available :(");
	}

	var renderer;
	var scene;
	var camera;
	
	var animations = [];
	
	// Load sounds 
	var listener = new THREE.AudioListener();
	var audioLoader = new THREE.AudioLoader();
	var load = ( path ) => {
		var sound = new THREE.PositionalAudio( listener );
		
		audioLoader.load( path, function( buffer ) {
			sound.setBuffer( buffer );
			sound.setRefDistance( 2 );
			sound.setLoop( true );
			sound.setRolloffFactor( 1 );
			sound.play();
			}
		);
		
		return sound;
	};
	
	var badCatSound = load( "Bad_Cat.ogg" );
	var dogSound = load( "dog.ogg" );
	
	// Create materials
		// The object used to load textures
	var textureLoader = new THREE.TextureLoader( );
		// Se ignora el callback de carga de texturas ya que la escena se renderiza de forma continua
	
	var regularFaceMaterial = new THREE.MeshPhongMaterial({ 
		map: textureLoader.load( "color.jpeg" ),
		bumpMap: textureLoader.load( "bump.jpeg" ),
		bumpScale: 0.1
		});
	var specialFaceMaterial = new THREE.MeshPhongMaterial({ 
		map: textureLoader.load( "buttonColor.png" ),
		bumpMap: textureLoader.load( "buttonBump.png" ),
		bumpScale: 0.1
		});
	
	var cubeMaterials = [
		specialFaceMaterial,
		regularFaceMaterial,
		regularFaceMaterial,
		regularFaceMaterial,
		regularFaceMaterial,
		regularFaceMaterial,
	];
	
	// Define meshes
	var createCubeMesh = () => {
		return new THREE.Mesh(
			new THREE.BoxGeometry( 1, 1, 1 ),
			cubeMaterials
			);
	};
		
	// Build scene
	scene = new THREE.Scene();
	
		// setup lights
	var light = new THREE.PointLight( 0xffffff, 1.6, 100 );
	light.position.set( 0, 0.5, 2 );
	scene.add( light );
	
	var hemiLight = new THREE.HemisphereLight( 0xffffff, 0xf0f0f0, 0.6 );
	hemiLight.position.set( 0, 500, 0 );
	scene.add( hemiLight );
	
		// setup cubes
	var createCube = (x, y, z, sound) => {
		var cube = new THREE.Object3D( );
		cube.position.set( x, y, z );
		cube.rotation.set( 0, Math.PI, 0 );
		
		var mesh = createCubeMesh();
		mesh.add( sound );
		cube.add( mesh );
		
		animations.push( ( delta ) => {
			
			// Animate cube
			var cubeRotation = ( delta * Math.PI * 2 ) / 4;
			cube.rotation.y += cubeRotation;
			cube.rotation.z += cubeRotation * 1.5;
		} );
		
		return cube;
	};
	
	var group = new THREE.Object3D( );
	group.add( createCube( 0, 0.5, 5, badCatSound ) );
	group.add( createCube( 0, 0.5, -5, dogSound ) );
	//group.add( createCube( 5, 0.5, 0 ) );
	//group.add( createCube( -5, 0.5, 0 ) );
	//group.add( createCube( 5, 0.5, 5 ) );
	//group.add( createCube( -5, 0.5, -5 ) );
	//group.add( createCube( -5, 0.5, 5 ) );
	//group.add( createCube( 5, 0.5, -5 ) );
	
	animations.push( ( delta ) => {
		var groupRotation = ( delta * Math.PI * 2 ) / 2;
		group.rotation.y += groupRotation;
	} );
	
	scene.add( group );
	
		// setup background grid
	var helper = new THREE.GridHelper( 100, 100, 0x744444, 0x444444 );
	helper.position.y = 0.1;
	scene.add( helper );
	
	// Build GUI
		// Mostrar framerate
	var stats = new Stats( );
	stats.domElement.style.position = 'absolute';
	stats.domElement.style.top = '0px';
	document.body.appendChild( stats.domElement );
	
	// Create renderer and append it to Dom 
	renderer = webGlAvailable ? 
		new THREE.WebGLRenderer({
			antialias: true
		}) 
		: new THREE.CanvasRenderer();
		
	renderer.setSize(window.innerWidth, window.innerHeight);
	document.body.appendChild(renderer.domElement);
	
	// Setup camera
	camera = new THREE.PerspectiveCamera(
		45, // FOV
		window.innerWidth / window.innerHeight, // Aspect ratio
		1, // Near plane distance
		4000 // Far plane distance
		);
	
	camera.position.set( 0, 1, 0 );
	camera.add( listener );
	
	var controls = new THREE.FirstPersonControls( camera );
	controls.movementSpeed = 2;
	controls.lookSpeed = 0.1;
	controls.noFly = false;
	controls.lookVertical = false;
	
	// Render
	renderer.render(scene, camera);
	
	// Add window resize hook
	var windowResize = new THREEx.WindowResize( renderer, camera, document.body );
	
	var clock = new THREE.Clock();
	
	// Add animation hook
	function animate () {
		
		requestAnimationFrame( animate );
		
		var delta = clock.getDelta();
		
		// Update stats
		stats.update( );
		
		// Update controls
		controls.update( delta );
		
		// Update other elements
		animations.forEach( ( item, index, array ) => item( delta ) );
		
		renderer.render( scene, camera );
	};
	animate();
});