(window.onload = () => {
	
	// The usage of Notepad++ is recommended along with bracket collapsing
	
	"use strict"; // We always use strict mode

	var webGlAvailable = WEBGL.isWebGLAvailable() 
	if (!webGlAvailable) {
		alert("WebGL not available :(");
	}

	var renderer;
	var scene;
	var camera;
	
	var onUpdate = [];
	var selectables = [];
	
	// Load sounds 
	var listener = new THREE.AudioListener();
	var load; // Parameter: path
	{
		var audioLoader = new THREE.AudioLoader();
		var load = ( path ) => {
			var sound = new THREE.PositionalAudio( listener );
			
			audioLoader.load( path, function( buffer ) {
				sound.setBuffer( buffer );
				sound.setRefDistance( 2 );
				sound.setLoop( true );
				sound.setRolloffFactor( 1 );
				sound.play();
				}
			);
			
			return sound;
		};
	}
	
	// Create materials
	var cubeMaterials;
	{
		// The object used to load textures
		var textureLoader = new THREE.TextureLoader( );
		// Se ignora el callback de carga de texturas ya que la escena se renderiza de forma continua
		
		var regularFaceMaterial = new THREE.MeshPhongMaterial({ 
			map: textureLoader.load( "color.jpeg" ),
			bumpMap: textureLoader.load( "bump.jpeg" ),
			bumpScale: 0.1
			});
		var specialFaceMaterial = new THREE.MeshPhongMaterial({ 
			map: textureLoader.load( "buttonColor.png" ),
			bumpMap: textureLoader.load( "buttonBump.png" ),
			bumpScale: 0.1
			});
		
		cubeMaterials = [
			specialFaceMaterial,
			regularFaceMaterial,
			regularFaceMaterial,
			regularFaceMaterial,
			regularFaceMaterial,
			regularFaceMaterial,
		];
	}
	
	// Define meshes
	var createCubeMesh; 
	{
		var createCubeMesh = (  ) => {
			return new THREE.Mesh(
				new THREE.BoxGeometry( 1, 1, 1 ),
				cubeMaterials
				);
		};
	}
		
	// Build scene
	{
		scene = new THREE.Scene();
		
		// setup lights
		var light = new THREE.PointLight( 0xffffff, 1.6, 100 );
		light.position.set( 0, 0.5, 2 );
		scene.add( light );
		
		var hemiLight = new THREE.HemisphereLight( 0xffffff, 0xf0f0f0, 0.6 );
		hemiLight.position.set( 0, 500, 0 );
		scene.add( hemiLight );
		
		// setup cubes
		var createCube = (x, y, z, name, sound) => {
			var cube = new THREE.Object3D( );
			cube.position.set( x, y, z );
			cube.rotation.set( 0, Math.PI, 0 );
			
			var mesh = createCubeMesh( );
			cube.add( mesh );
			
			mesh.name = name;
			mesh.add( sound );
			selectables.push( mesh );
			
			onUpdate.push( ( delta ) => {
				
				// Animate cube
				var cubeRotation = ( delta * Math.PI * 2 ) / 4;
				cube.rotation.y += cubeRotation;
				cube.rotation.z += cubeRotation * 1.5;
			} );
			
			return cube;
		};
		
		var group = new THREE.Object3D( );
		group.add( createCube( 0, 0.5, 5, "Bad cat", load( "Bad_Cat.ogg" ) ) );
		group.add( createCube( 0, 0.5, -5, "Good dog", load( "dog.ogg" ) ) );
		
		onUpdate.push( ( delta ) => {
			var groupRotation = ( delta * Math.PI * 2 ) / 20;
			group.rotation.y += groupRotation;
		} );
		
		scene.add( group );
		
		// setup background grid
		var helper = new THREE.GridHelper( 100, 100, 0x744444, 0x444444 );
		helper.position.y = 0.1;
		scene.add( helper );
	}
	
	// Setup player
	{
		camera = new THREE.PerspectiveCamera(
			45, // FOV
			window.innerWidth / window.innerHeight, // Aspect ratio
			1, // Near plane distance
			4000 // Far plane distance
			);
		
		camera.position.set( 0, 1, 0 );
		camera.add( listener );
		
		var controls = new THREE.FirstPersonControls( camera );
		controls.movementSpeed = 2;
		controls.lookSpeed = 0.1;
		controls.noFly = false;
		controls.lookVertical = false;
		onUpdate.push( ( delta ) => controls.update( delta ) );
	}
	
	// Setup renderer
	{
		renderer = webGlAvailable ? 
			new THREE.WebGLRenderer({
				antialias: true
			}) 
			: new THREE.CanvasRenderer();
			
		renderer.setSize(window.innerWidth, window.innerHeight);
		document.body.appendChild(renderer.domElement);
		
		
		// Add window resize hook
		var windowResize = new THREEx.WindowResize( renderer, camera, document.body );
		
		onUpdate.push( ( delta ) => renderer.render( scene, camera ) );
	}
	
	// Setup mouse selection
	{
		var rayCaster = new THREE.Raycaster();
		var mouse = new THREE.Vector2();
		var intersectedObject = null;
		
		function onDocumentMouseMove( event ) {

			event.preventDefault();
			mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
			mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;
		};
		
		document.addEventListener( 'mousemove', onDocumentMouseMove, false );

		onUpdate.push( ( delta ) => {
			rayCaster.setFromCamera( mouse, camera );
			rayCaster.linePrecision = 0.05; // My world is small!! Default = 1
			
			// Look for all the intersected objects
			var intersects = rayCaster.intersectObjects( selectables );
			if ( intersects.length > 0 ) {

				// Sorted by Z (close to the camera)
				if ( intersectedObject != intersects[ 0 ].object ) 
				{
					intersectedObject = intersects[ 0 ].object;
					console.log( 'New intersected object: ' + intersectedObject.name );
				}
			} 
			else 
			{
				intersectedObject = null;
			}
		} );
	}
	
	var clock = new THREE.Clock();
	
	// Add animation hook
	function animate () {
		
		requestAnimationFrame( animate );
		
		// Update other elements
		var delta = clock.getDelta();
		onUpdate.forEach( ( item, index, array ) => item( delta ) );
	};
	animate();
});