(window.onload = () => {
	
	"use strict"; // We always use strict mode

	var webGlAvailable = WEBGL.isWebGLAvailable() 
	if (!webGlAvailable) {
		alert("WebGL not available :(");
	}

	var renderer;
	var scene;
	var camera;
	
	// Create materials
		// The object used to load textures
	var textureLoader = new THREE.TextureLoader( );
		// Se ignora el callback de carga de texturas ya que la escena se renderiza de forma continua
	
	var cubeMaterial = new THREE.MeshPhongMaterial({ 
		map: textureLoader.load( "color.jpeg" ),
		bumpMap: textureLoader.load( "bump.jpeg" ),
		bumpScale: 0.1
		});
	
	// Create meshes
	var cubeMesh = new THREE.Mesh(
		new THREE.CubeGeometry( 1, 1, 1 ),
		cubeMaterial
		);
		
	// Build scene
	scene = new THREE.Scene();
	
		// setup light
	var light = new THREE.PointLight( 0xffffff, 1.6, 100 );
	light.position.set( -10, 0, 10 );
	scene.add( light );
	
		// setup cube
	scene.add( cubeMesh );
	
	// Create renderer and append it to Dom 
	renderer = webGlAvailable ? 
		new THREE.WebGLRenderer({
			antialias: true
		}) 
		: new THREE.CanvasRenderer();
		
	renderer.setSize(window.innerWidth, window.innerHeight);
	document.body.appendChild(renderer.domElement);
	
	// Create camera
	camera = new THREE.PerspectiveCamera(
		45, // FOV
		window.innerWidth / window.innerHeight, // Aspect ratio
		1, // Near plane distance
		4000 // Far plane distance
		);
		
	camera.position.set(0, 0, 5);
	
	// Render
	renderer.render(scene, camera);
	
	// Add window resize hook
	var windowResize = new THREEx.WindowResize( renderer, camera, document.body );
	
	var clock = new THREE.Clock();
	
	// Add animation hook
	function animate() {
		requestAnimationFrame( animate );

		var delta = clock.getDelta();
		
		// Animate cube
		var cubeRotation = ( delta * Math.PI * 2 ) / 24;
		cubeMesh.rotation.y += cubeRotation;
		
		renderer.render( scene, camera );
	};
	animate();
});