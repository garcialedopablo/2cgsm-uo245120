(window.onload = () => {
	
	var video = document.querySelector( 'video' );
	
	var constraints = {
		audio: false,
		video: { width: { exact: 640 }, height: { exact: 480 } }
	};
	
	navigator.mediaDevices.getUserMedia( constraints )
		// Called when we get the requested streams
		.then( function( stream ) {

			// Video tracks (usually only one)
			var videoTracks = stream.getVideoTracks( );
			console.log( 'Stream characteristics: ', constraints );
			console.log( 'Using device: ' + videoTracks[0].label );

			// End of stream handler
			stream.onended = function() {

				console.log( 'End of stream' );
			};

			// Bind the stream to the html video element
			video.srcObject = stream;
		})
		// Called in case of error
		.catch( function( error ) {

			if ( error.name === 'ConstraintNotSatisfiedError' ) {

				console.error( 
					'The resolution ' + constraints.video.width.exact + 'x' +
					 constraints.video.width.exact + ' px is not supported by the camera.' 
					 );
			} else if ( error.name === 'PermissionDeniedError' ) {

				console.error( 
					'The user has not allowed the access to the camera and the microphone.' 
					);
			}
			console.error( ' Error in getUserMedia: ' + error.name, error );
		});
		
	// Permitir guardado de imágenes
	{
		var streaming = false,
			width = constraints.video.width.exact; // Use previously set configuration
			height = constraints.video.height.exact;
			
		// Resize elements when the video is ready
		video.addEventListener( 'canplay', function( ev ) {

			if ( !streaming ) {  // To prevent re-entry
			
				video.setAttribute( 'width', width );
				video.setAttribute( 'height', height );
				canvas.setAttribute( 'width', width );
				canvas.setAttribute( 'height', height );
				streaming = true;
			}
		}, false );

		// Program the "capture" link to extract and download the current frame
		var canvas = document.querySelector( 'canvas' );          // Select by element type
		var captureButton = document.getElementById( 'capture' ); // Select by id

		captureButton.addEventListener( 'click', function( ev ) {
			
			canvas.width = width;
			canvas.height = height;
			canvas.getContext( '2d' ).drawImage( video, 0, 0, width, height );

			// Start downloading (thanks to the 'download' attribute of the link)
			var dataURL = canvas.toDataURL( 'image/png' );
			captureButton.href = dataURL;
			
		}, false );
	}
})