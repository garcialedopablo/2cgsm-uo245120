(window.onload = () => {
	
	"use strict"; // We always use strict mode

	var webGlAvailable = WEBGL.isWebGLAvailable() 
	if (!webGlAvailable) {
		alert("WebGL not available :(");
	}

	var renderer;
	var scene;
	var camera;
	
	var onUpdate = [];
	
	var video = document.getElementById( 'video' );
	var width = 854;
	var height = 480;
	
	// Create materials
	var imageContext;
	var planeMaterial;
	{
		// Setup plane canvas and texture
		var image = document.createElement( 'canvas' );
		image.width = width;  // Video width
		image.height = height; // Video height
		
		imageContext = image.getContext( '2d' );
		imageContext.fillStyle = '#000000';
		imageContext.fillRect( 0, 0, width - 1, height - 1 );
		
		var texture = new THREE.Texture( image );
		
		// Create material
		planeMaterial = new THREE.MeshBasicMaterial({ 
			map: texture,
			side: THREE.DoubleSide
			});
			
		// Render DASH playback on video element
		var url = "http://192.168.1.80:8080/sintel.mpd";
		var player = dashjs.MediaPlayer( ).create( );
		player.initialize( video, url, true );
			
		// Animate material with video element contents
		onUpdate.push( ( delta ) => {
			
			if ( video.readyState === video.HAVE_ENOUGH_DATA ) {

				imageContext.drawImage( video, 0, 0 );
				if ( texture ) texture.needsUpdate = true;
			}
		} );
	}
	
	// Setup dash playback
	{
		var url = "";
	}
	
	// Create meshes
	var planeMesh;
	{
		planeMesh = new THREE.Mesh(
			new THREE.PlaneGeometry( width, height, 4, 4 ),
			planeMaterial
			);
	}
	
	// Build scene
	{
		scene = new THREE.Scene();
		
		// setup light
		var light = new THREE.PointLight( 0xffffff, 1.6, 50000 );
		light.position.set( -2000, 0, 2000 );
		scene.add( light );
		
		// setup plane
		scene.add( planeMesh );
		
		// Animate plane
		onUpdate.push( ( delta ) => {
			
			var planeRotation = ( delta * Math.PI * 2 ) / 24;
			planeMesh.rotation.y += planeRotation;
		} );
	}
	
	// Create renderer and append it to Dom 
	{
		renderer = webGlAvailable ? 
			new THREE.WebGLRenderer({
				antialias: true
			}) 
			: new THREE.CanvasRenderer();
			
		renderer.setSize(window.innerWidth, window.innerHeight);
		document.body.appendChild(renderer.domElement);
	}
	
	// Create camera
	{
		camera = new THREE.PerspectiveCamera(
			45, // FOV
			window.innerWidth / window.innerHeight, // Aspect ratio
			1, // Near plane distance
			4000 // Far plane distance
			);
			
		camera.position.set(0, 0, 500);
	}
	
	// Render
	renderer.render(scene, camera);
	
	// Add window resize hook
	var windowResize = new THREEx.WindowResize( renderer, camera, document.body );
	
	var clock = new THREE.Clock();
	
	// Add animation hook
	function animate() {
		requestAnimationFrame( animate );
		
		var delta = clock.getDelta();
		onUpdate.forEach( ( f ) => f( delta ) );
		
		renderer.render( scene, camera );
	};
	animate();
});