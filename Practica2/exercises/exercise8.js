(window.onload = () => {
	
	"use strict"; // We always use strict mode

	var webGlAvailable = WEBGL.isWebGLAvailable() 
	alert(`WebGL availability: ${webGlAvailable}`);

	var renderer;
	var scene;
	var camera;
	
	// Load textures
	var textureLoader = new THREE.TextureLoader( );  // The object used to load textures
	var earthMap = textureLoader.load( "earth.jpeg" );
	var atmosphereMap = textureLoader.load( "atmosphere.png" );
	var moonMap = textureLoader.load( "moon.jpeg" );
	
	// Se ignora el callback de carga de texturas ya que la escena se renderiza de forma continua
	
	// Create materials
	var earthMaterial = new THREE.MeshPhongMaterial({ 
		map: earthMap 
		});
	var atmosphereMaterial = new THREE.MeshLambertMaterial({ 
		color: 0xFFFFFF,
		map: atmosphereMap,
		transparent: true
		});
	var moonMaterial = new THREE.MeshLambertMaterial({ 
		color: 0x888888,
		map: moonMap
		});
	
	// Create meshes
	var earthMesh = new THREE.Mesh(
		new THREE.SphereGeometry( 1, 16, 16 ),
		earthMaterial
		);
	var atmosphereMesh = new THREE.Mesh(
		new THREE.SphereGeometry( 1.06, 16, 16 ),
		atmosphereMaterial
		);
	var moonMesh = new THREE.Mesh(
		new THREE.SphereGeometry( 0.27, 16, 16 ),
		moonMaterial
		);
	var iss;
		
	// Build scene
	scene = new THREE.Scene();
	
		// setup light
	var light = new THREE.PointLight( 0xffffff, 1.6, 100 );
	light.position.set( -10, 0, 10 );
	scene.add( light );
	
		// setup earth
	var earth = new THREE.Object3D();
	earth.add( atmosphereMesh );
	earth.add( earthMesh );
	earth.rotation.set( 0, 0, 0.36 );
	scene.add( earth );
	
		// setup moon
	var moonDistance = 384402 / 6371;
	moonMesh.position.set( 
		Math.sqrt( moonDistance / 2 ), 
		0, 
		-Math.sqrt( moonDistance / 2 )
		);
	moonMesh.rotation.y = Math.PI;
	
	var moon = new THREE.Object3D( );
	moon.rotation.x = 0.089;
	moon.add( moonMesh );
	
	scene.add( moon );
	
		// define ISS setup
	var setupIss = new THREE.LoadingManager( 
		function() {
		
			iss.scale.x = iss.scale.y = iss.scale.z = 0.005;
			iss.rotation.set( Math.PI / 5, Math.PI / 5, 0 );
			iss.position.set( 0, 0, 1.5 );
			iss.updateMatrix( );
			
			earth.add( iss );
		} );
	
	// Load ISS model
	var loader = new THREE.ColladaLoader( setupIss );
	loader.load( 
		"iss/models/iss.dae", 
		function ( collada ) {
			iss = collada.scene;
		} );
	
	// Create renderer and append it to Dom 
	renderer = webGlAvailable ? 
		new THREE.WebGLRenderer({
			antialias: true
		}) 
		: new THREE.CanvasRenderer();
		
	renderer.setSize(window.innerWidth, window.innerHeight);
	document.body.appendChild(renderer.domElement);
	
	// Create camera
	camera = new THREE.PerspectiveCamera(
		45, // FOV
		window.innerWidth / window.innerHeight, // Aspect ratio
		1, // Near plane distance
		4000 // Far plane distance
		);
		
	camera.position.set(0, 0, 10);
	
	// Render
	renderer.render(scene, camera);
	
	// Add window resize hook
	var windowResize = new THREEx.WindowResize( renderer, camera, document.body );
	
	var clock = new THREE.Clock();
	
	// Add animation hook
	function animate() {
		requestAnimationFrame( animate );

		var delta = clock.getDelta();
		
		// Animate earth
		var earthRotation = ( delta * Math.PI * 2 ) / 24;
		earth.rotation.y += earthRotation;
		atmosphereMesh.rotation.y -= earthRotation * 0.1;
		
		// Animate moon
		var moonRotation = ( delta * Math.PI * 2 ) / ( 24 * 28 );
		moon.rotation.y += moonRotation;

		renderer.render( scene, camera );
	};
	animate();
});