(window.onload = () => {
	
	"use strict"; // We always use strict mode

	var webGlAvailable = WEBGL.isWebGLAvailable() 
	alert(`WebGL availability: ${webGlAvailable}`);

	var renderer;
	var scene;
	var camera;
	
	// Load textures
	var textureLoader = new THREE.TextureLoader( );  // The object used to load textures
	var earthMap = textureLoader.load( "earth.jpeg" );
	var atmosphereMap = textureLoader.load( "atmosphere.png" );
	var moonMap = textureLoader.load( "moon.jpeg" );
	
	// Se ignora el callback de carga de texturas ya que la escena se renderiza de forma continua
	
	// Create materials
	var earthMaterial = new THREE.MeshPhongMaterial({ map: earthMap });
	
	// Create meshes
	var earthMesh = new THREE.Mesh(
		new THREE.SphereGeometry( 1, 16, 16 ),
		earthMaterial
		);
	
	// Build scene
	scene = new THREE.Scene();
	
	var light = new THREE.PointLight( 0xffffff, 1.6, 100 );
	light.position.set( -4, 0, 4 );
	scene.add( light );
	
	scene.add( earthMesh );
	
	// Create renderer and append it to Dom 
	renderer = webGlAvailable ? 
		new THREE.WebGLRenderer({
			antialias: true
		}) 
		: new THREE.CanvasRenderer();
		
	renderer.setSize(window.innerWidth, window.innerHeight);
	document.body.appendChild(renderer.domElement);
	
	// Create camera
	camera = new THREE.PerspectiveCamera(
		45, // FOV
		window.innerWidth / window.innerHeight, // Aspect ratio
		1, // Near plane distance
		4000 // Far plane distance
		);
		
	camera.position.set(0, 0, 10);
	
	// Render
	renderer.render(scene, camera);
	
	// Add window resize hook
	var windowResize = new THREEx.WindowResize( renderer, camera, document.body );
	
	// Add animation hook
	function animate() {
		requestAnimationFrame( animate );

		renderer.render( scene, camera )
	};
	animate();
});