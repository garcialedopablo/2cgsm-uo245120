(window.onload = () => {
		
	"use strict"; // We always use strict mode

	var webGlAvailable = WEBGL.isWebGLAvailable() 
	alert(`WebGL availability: ${webGlAvailable}`);

	var renderer;
	var scene;
	var camera;
		
	var buildHome = () => {
		var homeGeometry = new THREE.Geometry();
	
		// Rectangle bottom 0-1
		homeGeometry.vertices.push( new THREE.Vector3(-0.5, -0.4, 0) );
		homeGeometry.vertices.push( new THREE.Vector3(0.5, -0.4, 0) );
		// Rectangle top 2-3
		homeGeometry.vertices.push( new THREE.Vector3(-0.5, 0.1, 0) );
		homeGeometry.vertices.push( new THREE.Vector3(0.5, 0.1, 0) );
		// Door bottom 4-5
		homeGeometry.vertices.push( new THREE.Vector3(0, -0.4, 0) );
		homeGeometry.vertices.push( new THREE.Vector3(0.1, -0.4, 0) );
		// Door top 6-7
		homeGeometry.vertices.push( new THREE.Vector3(0, -0.2, 0) );
		homeGeometry.vertices.push( new THREE.Vector3(0.1, -0.2, 0) );
		// Triangle bottom 8-9
		homeGeometry.vertices.push( new THREE.Vector3(-0.6, 0.1, 0) );
		homeGeometry.vertices.push( new THREE.Vector3(0.6, 0.1, 0) );
		// Triangle top 10
		homeGeometry.vertices.push( new THREE.Vector3(0, 0.3, 0) );
		
		// "Rectangle" left faces
		homeGeometry.faces.push( new THREE.Face3( 0, 4, 2 ) );
		homeGeometry.faces.push( new THREE.Face3( 2, 4, 6 ) );
		// "Rectangle" top faces
		homeGeometry.faces.push( new THREE.Face3( 2, 6, 7 ) );
		homeGeometry.faces.push( new THREE.Face3( 2, 7, 3 ) );
		// "Rectangle" right faces
		homeGeometry.faces.push( new THREE.Face3( 7, 1, 3 ) );
		homeGeometry.faces.push( new THREE.Face3( 7, 5, 1 ) );
		// Triangle face
		homeGeometry.faces.push( new THREE.Face3( 8, 9, 10 ) );
		
		homeGeometry.computeFaceNormals( );
		
		return homeGeometry;
	};
	
	// Create scene
	scene = new THREE.Scene();
	
	// Add home in the center
	var homeMaterial = new THREE.MeshBasicMaterial()
	homeMaterial.color = new THREE.Color(0x00d0d0);
	
	var home = new THREE.Mesh(
		buildHome(), 
		homeMaterial,
		);
		
	scene.add(home);
	
	// Add orbiting and spinning satellite
	// Satellite
	var satellite = new THREE.Object3D();
	
	var sphere = new THREE.Mesh(
		new THREE.SphereGeometry(0.35, 4, 4), 
		new THREE.MeshStandardMaterial()
		);
	satellite.add(sphere);
	
	var cone = new THREE.Mesh(
		new THREE.ConeGeometry(0.25, 0.5, 10),
		new THREE.MeshNormalMaterial()
		);
	cone.rotation.set(0, 0, -Math.PI / 2);
	cone.position.set(1, 0, 0);
	satellite.add(cone);
	
	satellite.position.set(3, 0, 0);
	
	// Orbit center
	var center = new THREE.Object3D();
	center.add(satellite);
	scene.add(center);
	
	// Lights
	var ambient = new THREE.AmbientLight( 0x404040 );
	var point = new THREE.PointLight( 0x00d0d0, 1, 100 );
	point.position.set(0, 0, 3);
	scene.add(ambient);
	scene.add(point);
	
	// Create renderer and append it to Dom 
	renderer = webGlAvailable ? 
		new THREE.WebGLRenderer({
			antialias: true
		}) 
		: new THREE.CanvasRenderer();
		
	renderer.setSize(window.innerWidth, window.innerHeight);
	document.body.appendChild(renderer.domElement);
	
	// Create camera
	camera = new THREE.PerspectiveCamera(
		45, // FOV
		window.innerWidth / window.innerHeight, // Aspect ratio
		1, // Near plane distance
		4000 // Far plane distance
		);
		
	camera.position.set(0, 0, 8);
	
	// Render
	renderer.render(scene, camera);
	
	// Add window resize hook
	var windowResize = new THREEx.WindowResize( renderer, camera, document.body );
	
	// Add animation hook
	function animate() {
		requestAnimationFrame( animate );

		satellite.rotateZ(Math.PI / 120);
		center.rotateZ(Math.PI / 500);
		
		renderer.render( scene, camera )
	};
	animate();
});