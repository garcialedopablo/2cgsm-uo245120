(window.onload = () => {
		
	"use strict"; // We always use strict mode

	var webGlAvailable = WEBGL.isWebGLAvailable() 
	alert(`WebGL availability: ${webGlAvailable}`);

	var renderer;
	var scene;
	var camera;
	
	// Create scene
	scene = new THREE.Scene();
	
	var addObject = (geometry, x, y, z) => {
		var material = new THREE.MeshBasicMaterial();
		var object = new THREE.Mesh(geometry, material);
		
		// Rotate all objects
		object.rotation.set(Math.PI / 5, Math.PI / 5,  0); 
		object.position.set(x, y, z);
		
		scene.add(object);
	};
	
	var boxGeometry = new THREE.BoxGeometry(0.8, 0.8, 0.8);
	addObject(boxGeometry, 1, 0, 0);
	
	var sphereGeometry = new THREE.SphereGeometry(0.5, 10, 10);
	addObject(sphereGeometry, -1, 0, 0);
	
	var cylinderGeometry = new THREE.CylinderGeometry(0.4, 0.4, 0.9, 10);
	addObject(cylinderGeometry, 2, 0, 0);
	
	var coneGeometry = new THREE.ConeGeometry(0.5, 1, 10);
	addObject(coneGeometry, -2, 0, 0);
	
	var homeGeometry = new THREE.Geometry();
	// Rectangle bottom 0-1
	homeGeometry.vertices.push( new THREE.Vector3(-0.5, -0.4, 0) );
	homeGeometry.vertices.push( new THREE.Vector3(0.5, -0.4, 0) );
	// Rectangle top 2-3
	homeGeometry.vertices.push( new THREE.Vector3(-0.5, 0.1, 0) );
	homeGeometry.vertices.push( new THREE.Vector3(0.5, 0.1, 0) );
	// Door bottom 4-5
	homeGeometry.vertices.push( new THREE.Vector3(0, -0.4, 0) );
	homeGeometry.vertices.push( new THREE.Vector3(0.1, -0.4, 0) );
	// Door top 6-7
	homeGeometry.vertices.push( new THREE.Vector3(0, -0.2, 0) );
	homeGeometry.vertices.push( new THREE.Vector3(0.1, -0.2, 0) );
	// Triangle bottom 8-9
	homeGeometry.vertices.push( new THREE.Vector3(-0.6, 0.1, 0) );
	homeGeometry.vertices.push( new THREE.Vector3(0.6, 0.1, 0) );
	// Triangle top 10
	homeGeometry.vertices.push( new THREE.Vector3(0, 0.3, 0) );
	
	// "Rectangle" left faces
	homeGeometry.faces.push( new THREE.Face3( 0, 4, 2 ) );
	homeGeometry.faces.push( new THREE.Face3( 2, 4, 6 ) );
	// "Rectangle" top faces
	homeGeometry.faces.push( new THREE.Face3( 2, 6, 7 ) );
	homeGeometry.faces.push( new THREE.Face3( 2, 7, 3 ) );
	// "Rectangle" right faces
	homeGeometry.faces.push( new THREE.Face3( 7, 1, 3 ) );
	homeGeometry.faces.push( new THREE.Face3( 7, 5, 1 ) );
	// Triangle face
	homeGeometry.faces.push( new THREE.Face3( 8, 9, 10 ) );
	
	homeGeometry.computeFaceNormals( );
	
	addObject(homeGeometry, 0, 0, 0);
	
	// Create renderer and append it to Dom 
	renderer = webGlAvailable ? 
		new THREE.WebGLRenderer({
			antialias: true
		}) 
		: new THREE.CanvasRenderer();
		
	renderer.setSize(window.innerWidth, window.innerHeight);
	document.body.appendChild(renderer.domElement);
	
	// Create camera
	camera = new THREE.PerspectiveCamera(
		45, // FOV
		window.innerWidth / window.innerHeight, // Aspect ratio
		1, // Near plane distance
		4000 // Far plane distance
		);
		
	camera.position.set(0, 0, 5);
	
	// Render
	renderer.render(scene, camera);
	
	// Add window resize hook
	var windowResize = new THREEx.WindowResize( renderer, camera, document.body );
	
	// Add animation hook
	function animate() {
		requestAnimationFrame( animate );

		renderer.render( scene, camera )
	};
	animate();
});