(function () {
	
	"use strict"; // We always use strict mode

	var webGlAvailable = WEBGL.isWebGLAvailable() 
	alert(`WebGL availability: ${webGlAvailable}`);

	var renderer = webGlAvailable ? new THREE.WebGLRenderer() : new THREE.CanvasRenderer();
    
})();