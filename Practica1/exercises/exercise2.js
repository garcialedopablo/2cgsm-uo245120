(window.onload = () => {
	
	"use strict"; // We always use strict mode

	var webGlAvailable = WEBGL.isWebGLAvailable() 
	alert(`WebGL availability: ${webGlAvailable}`);

	var renderer;
	var scene;
	var camera;

	// Create scene
	scene = new THREE.Scene();
	
	var geometry = new THREE.BoxGeometry(1, 1, 1);
    var material = new THREE.MeshBasicMaterial();
    var box = new THREE.Mesh(geometry, material);

	scene.add(box);
	
	// Create renderer and append it to Dom 
	renderer = webGlAvailable ? 
		new THREE.WebGLRenderer({
			antialias: true
		}) 
		: new THREE.CanvasRenderer();
		
    renderer.setSize(window.innerWidth, window.innerHeight);
    document.body.appendChild(renderer.domElement);
	
	// Create camera
	camera = new THREE.PerspectiveCamera(
		45, // FOV
		window.innerWidth / window.innerHeight, // Aspect ratio
		1, // Near plane distance
		4000 // Far plane distance
		);
		
    camera.position.set(0, 0, 3);
	
	// Render
	renderer.render(scene, camera);
});